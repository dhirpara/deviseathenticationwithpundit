class User < ActiveRecord::Base
	enum role: [:admin, :user, :moderator, :supervisor]
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
	has_many :posts         
  after_initialize :set_default_role, :if => :new_record?
  
  def set_default_role
    self.role ||= :user
  end
end