class ApplicationController < ActionController::Base
 	include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

 	protect_from_forgery with: :exception

 	before_filter :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit( :email, :password, :password_confirmation,:remember_me, :role) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:email, :password, :remember_me) }
    devise_parameter_sanitizer.for(:account_update){ |u| u.permit(:username, :email, :password, :password_confirmation,:remember_me, :role) }
  end
  
  private

  def user_not_authorized
    flash[:alert] = "Access denied."
    redirect_to (request.referrer || root_path)
  end
end
