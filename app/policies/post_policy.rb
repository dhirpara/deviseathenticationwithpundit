class PostPolicy < ApplicationPolicy
  attr_reader :current_user, :model

  def initialize(current_user, model)
  	@current_user = current_user
  	@post = model
  end

  def new?
  	@current_user.admin? or @current_user.user?
  end

  def create?
  	@current_user.admin? or @current_user.user?
  end
  
  def destroy?
  	@current_user.admin? or @current_user.supervisor?
  end
end
